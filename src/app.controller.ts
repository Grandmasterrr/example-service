import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  @Get('hello')
  public handleHello() {
    return 'Hello World!';
  }
}
