FROM node:16.13.1-alpine
USER node
WORKDIR /app
COPY package.json .
COPY dist dist
COPY node_modules node_modules
ENV PORT 9000
EXPOSE $PORT
CMD ["node", "dist/main"]
