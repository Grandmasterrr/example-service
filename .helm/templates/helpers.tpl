{{- define "resourceIdentifier" -}}
{{- default .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "objects.deployment.name" -}}
{{- include "resourceIdentifier" . }}
{{- end }}

{{- define "objects.container.name" -}}
{{- .Values.werf.name }}-app
{{- end }}

{{- define "objects.pod.type" -}}
{{- include "resourceIdentifier" . }}-pod
{{- end }}

{{- define "objects.deployment.name" -}}
{{- include "resourceIdentifier" . }}
{{- end }}

{{- define "objects.service.name" -}}
{{- include "resourceIdentifier" . }}-svc
{{- end }}

{{- define "objects.registryReader.name" -}}
{{- include "resourceIdentifier" . }}-reg-secret
{{- end }}
